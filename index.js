const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');

// Example of send fax with multipart/form-data headers
async function sendFax() {
    const token = 'API KEY';
    const file1 = await fs.createReadStream('./sample.pdf');
    const file2 = await fs.createReadStream('./sample.pdf');

    const faxBody = {
        faxNumber: 'NUMBER',
        coverPage: 'false',
        'attachments[]': [file1, file2],
        'attachmentUrls[]': ['https://www.africau.edu/images/default/sample.pdf'],
    };
    const formData = new FormData();
    for (const field in faxBody) {
        const data = faxBody[field];
        if (Array.isArray(data)) {
            data.forEach(element => {
                formData.append(field, element);
            });
        } else {
            formData.append(field, data);
        }
    };

    const axiosRequestOpts = {
        url: 'https://api.documo.com/v1/faxes',
        method: 'POST',
        headers: {
            'Authorization': `Api ${token}`,
            ...formData.getHeaders(),
        },
        data: formData,
    };

    try {
        const sendFaxApiCall = await axios(axiosRequestOpts);
        console.log('Response: ', sendFaxApiCall);
    } catch(err) {
        console.error('Response: ', err.response.data);
    };
}

// sendFax();

// Example of send fax with application/json headers
async function sendFaxJSON() {
    const token = 'API KEY';

    const faxBody = {
        faxNumber: 'NUMBER',
        coverPage: 'false',
        attachmentUrls: ['https://www.africau.edu/images/default/sample.pdf'],
    };

    const axiosRequestOpts = {
        url: 'https://api.documo.com/v1/faxes',
        method: 'POST',
        headers: {
            'Authorization': `Api ${token}`,
        },
        data: faxBody,
    };

    try {
        const sendFaxApiCall = await axios(axiosRequestOpts);
        console.log('Response: ', sendFaxApiCall);
    } catch(err) {
        console.error('Response: ', err.response.data);
    };
}

sendFaxJSON()